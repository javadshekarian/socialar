var online = new Object();
var all_sockets = new Object();

const {messageInsertOne,messageFindMany, messageInsertMany,messageUpdateMany} = require('../usage/message');
const {uploadFindOne} = require('../usage/upload');
const {Upload} = require('../model/Upload');
const {generate} = require('shortid');

module.exports.socketServer = (io) =>{
    return io.on('connection',socket=>{

        socket.on('online',data=>{
            online[data.user_id] = socket.id;
            all_sockets[data.user_id] = socket;
        })

        socket.on('message',async data=>{
            try{
                const {from,to,sender_username,receiver_username,message,time} = data;
                if(online[data.to]){
                    socket.broadcast.emit('message',{
                        from,to,sender_username,receiver_username,message,time,type:"text"
                    })
                }
                var index = new Object();
                index['from']              = from;
                index['to']                = to;
                index['sender_username']   = sender_username;
                index['receiver_username'] = receiver_username;
                index['message']           = message;
                index['time']              = time;
                index['type']              = 'text';

                await messageInsertOne(index);
            }catch(err){
                socket.emit('err',{err});
            }
        })

        socket.on('send-post',async data=>{
            try{
                const {message,time,from,sender_username} = data;
                const type = 'post';
                const data_array = new Array();
                Object.keys(data.sendList).forEach(async username=>{
                    const index = new Object();
                    index['message'] = message;
                    index['time'] = time;
                    index['sender_username'] = sender_username;
                    index['from'] = from;
                    index['type'] = type;
                    index['receiver_username'] = username;
                    index['to'] = data.sendList[username];
                    index['reference'] = message;
                    data_array.push(index);
                });

                const room_name = generate();

                Object.keys(data.sendList).forEach(async (username) => {
                    var user_id = data.sendList[username];
                    if(!!online[user_id]) await all_sockets[user_id].join(room_name);
                })

                var videoName = await Upload.find({_id:message}).populate({path:'reference'});

                io.to(room_name).emit('receive_post',{
                    message,time,from,sender_username,type,reference:videoName[0]
                })

                await messageInsertMany(data_array);
            }catch(err){
                socket.emit('err',{err});
            }
        })

        socket.on('disconnect',()=>{
            delete online[socket.id];
        })
    })
}