const mongoose = require('mongoose');

const MessageSchema = mongoose.Schema({
    from:{
        type:String,
        require:true
    },
    to:{
        type:String,
        require:true
    },
    message:{
        type:String,
        require:true
    },
    time:{
        type:Number,
        require:true
    },
    type:{
        type:String,
        require:true
    },
    sender_username:{
        type:String,
        require:true
    },
    receiver_username:{
        type:String,
        require:true
    },
    reference:{
        type:mongoose.Types.ObjectId,
        ref:"uploads"
    }
},{
    timestamps:true
});

const Message = mongoose.model('messages',MessageSchema);
module.exports.Message = Message;