const mongoose = require('mongoose');

const FollowerSchema = mongoose.Schema({
    user_id:{
        type:String,
        require:true
    },
    followers:[String]
},{
    timestamps:true
});

const Follower = mongoose.model('followers',FollowerSchema);
module.exports.Follower = Follower;