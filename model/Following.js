const mongoose = require('mongoose');

const FollowingSchema = mongoose.Schema({
    user_id:{
        type:String,
        require:true
    },
    following:[String]
},{
    timestamps:true
})

const Following = mongoose.model('followings',FollowingSchema);
module.exports.Following = Following;

