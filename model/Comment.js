const mongoose = require('mongoose');

const CommentSchema = mongoose.Schema({
    post_id:{
        type:String,
        require:true
    },
    comment:{
        type:String,
        require:true
    },
    user_id:{
        type:String,
        require:true
    },
    username:{
        type:String,
        require:true
    },
    reference:{
        type:mongoose.Types.ObjectId,
        ref:"users"
    }
},{
    timestamps:true
});

const Comment = mongoose.model('comments',CommentSchema);
module.exports.Comment = Comment;