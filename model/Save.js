const mongoose = require('mongoose');

const SaveSchema = mongoose.Schema({
    user_id:{
        type:String,
        require:true
    },
    my_saves:[String]
},{
    timestamps:true
})

const Save = mongoose.model('saves',SaveSchema);
module.exports.Save = Save;