const mongoose = require('mongoose');

const UploadSchema = mongoose.Schema({
    name:{
        type:String,
        require:true
    },
    username:{
        type:String,
        require:true
    },
    email:{
        type:String,
        require:true
    },
    user_id:{
        type:String,
        require:true
    }
    ,
    type:{
        type:String,
        default:'img'
    },
    reference:{
        type:mongoose.Types.ObjectId,
        ref:"users"
    }
},
{ 
    timestamps: true 
}
);

const Upload = mongoose.model('uploads',UploadSchema);

module.exports.Upload = Upload;