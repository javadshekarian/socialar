const mongoose = require('mongoose');

const LikeSchema = mongoose.Schema({
    post_id:{
        type:String,
        require:true
    },
    likes:[String]
},{
    timestamps:true
})

const Like = mongoose.model('likes',LikeSchema);
module.exports.Like = Like;