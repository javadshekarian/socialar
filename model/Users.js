const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    password:{
        type:String,
        require:true
    },
    username:{
        type:String,
        require:true
    },
    email:{
        type:String,
        require:true
    },
    followers_id:{
        type:String
    },
    following_id:{
        type:String
    },
    profile_picture:{
        type:String
    }
},
{ 
    timestamps: true 
}
)

const User = mongoose.model('users',UserSchema);

module.exports.User = User;