import React from 'react';
import ReactDOM from 'react-dom/client';
import './css/index.css';
import App from './components/App';
import reportWebVitals from './components/reportWebVitals';
import Login from './components/Login';
import Register from './components/Register';
import Home from './components/Home';
import NotFound from './components/NotFound';
import CreateAR from './components/CreateAR';
import Explor from './components/Explor';
import ChatRoom from './components/ChatRoom';
import Messenger from './components/Messenger';
import Posts from './components/Posts';
import Profile from './components/Profile';
import PrivateRoute from './components/PrivateRoutes';
import VideoChromaKey from './components/VideoChromaKey';
import Test from './components/Test';
import Comment from './components/Comment';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import {io} from 'socket.io-client';
import { jwtDecode } from 'jwt-decode';

const socket = io('https://api.bychat.one',{
  autoConnect:true
});

socket.on('connect',()=>{
  socket.emit('online',{
    user_id:jwtDecode(sessionStorage.getItem('token')).user_id
  })
})

export default function Main() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Login socket={socket} />}></Route>
        <Route path="/register" element={<Register socket={socket} />}/>
        
        <Route path="/profile" element={
          <PrivateRoute>
            <Profile socket={socket}/>
          </PrivateRoute>
        }></Route>
        
        <Route path="/posts" element={
          <PrivateRoute>
            <Posts socket={socket}/>
          </PrivateRoute>
        }></Route>

        <Route path="/messenger" element={
          <PrivateRoute>
            <Messenger socket={socket}/>
          </PrivateRoute>
        }></Route>

        <Route path="/chatroom" element={
          <PrivateRoute>
            <ChatRoom socket={socket}/>
          </PrivateRoute>
        }></Route>

        <Route path="/explor" element={
          <PrivateRoute>
            <Explor socket={socket} />
          </PrivateRoute>
        }></Route>

        <Route path="/create-ar" element={
          <PrivateRoute>
            <CreateAR socket={socket} />
          </PrivateRoute>
        }></Route>

        <Route path="/home" element={
          <PrivateRoute>
            <Home socket={socket} />
          </PrivateRoute>
        }></Route>

        <Route path="/" element={
          <PrivateRoute>
            <App socket={socket} />
          </PrivateRoute>
        }></Route>

        <Route path="/videoChromaKey" element={
          <PrivateRoute>
            <VideoChromaKey socket={socket} />
          </PrivateRoute>
        }></Route>

        <Route path="/comment" element={
          <PrivateRoute>
            <Comment socket={socket} />
          </PrivateRoute>
        }></Route>

        <Route path="/test" element={<Test/>}></Route>
        <Route path='*' element={<NotFound />}/>
      </Routes>
    </BrowserRouter>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<Main />);
reportWebVitals();
