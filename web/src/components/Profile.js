import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/App.css'
import '../css/profile.css';

import {useNavigate,useLocation} from 'react-router-dom';
import {useEffect,useState,useRef} from 'react';
import axios from 'axios';

function Profile(){
    const navigate = useNavigate();
    const {state} = useLocation();
    console.log('state',state);
    const [posts,setPosts] = useState([]);
    const [flrs,setFlrs] = useState([]);
    const [flgs,setFlgs] = useState([]);
    const [isFollow,setIsFollow] = useState(false);
    const followTag = useRef(null);
    const [profile_info,setProfile_Info] = useState({});

    useEffect(()=>{
        var formdata = new FormData();
        formdata.append('id',state.id);

        var formdata2 = new FormData();
        formdata2.append('token',sessionStorage.getItem('token'));
        formdata2.append('id',state.id);

        axios.post(`https://api.bychat.one/follow/following-profile`,{
            user_id:state.id
        }).then(res=>{
            setProfile_Info(res.data)
        })

        axios.post(`https://api.bychat.one/upload/cantact-posts`,formdata).then(res=>{
            setPosts(res.data);
        })

        axios.post(`https://api.bychat.one/follow/is-follow`,formdata2).then(res=>{
            setIsFollow(Boolean(res.data.length));
        })

        axios.post(`https://api.bychat.one/follow/follower-numbers-decode`,{
            user_id:state.id
        })
        .then(res=>{
            setFlrs(res.data);
        })

        axios.post(`https://api.bychat.one/follow/following-numbers-decode`,{
            user_id:state.id
        })
        .then(res=>{
            setFlgs(res.data);
        })

    },[])

    const following = async () =>{
        var formdata = new FormData();
        formdata.append('token',sessionStorage.getItem('token'));
        formdata.append('user_id',state.id);

        var res = await axios.post(`https://api.bychat.one/follow/follow`,formdata);
        
        if(res.data.content === 'unfollow'){
            followTag.current.innerText = 'Follow';
            followTag.current.classList.add('bclogo');
        }else{
            followTag.current.innerText = 'unfollow';
            followTag.current.classList.remove('bclogo');
        }
    }

    const openPostPageSmallPost = (item,posts) =>{
        navigate('/posts',{state:{videoName:item.name,type:'public',posts:posts}});
      }

    const backToPosts = ()=> {
        navigate(-1);
    }

    const openMessageRoot = (username,user_id,profile_picture) => {
        navigate('/messenger',{state:{profile_picture,username,user_id}})
    }

    return(
        <div>
            <div className='frfs aic'>
                <i className='bi bi-arrow-left fs20 ml10 ddd' onClick={backToPosts}></i>
                <div className='fontrr fs16 ml10'>{state.username}</div>
            </div>
            <div className='frs w100 aic mt20'>
                {!!profile_info.profile_picture?
                <div className='profilePicInMain' style={{backgroundImage:`url(/profile_pictures/${profile_info.profile_picture})`}}></div>
                :
                <div className='profilePicInMain' style={{backgroundImage:'url(/profile_pictures/nonProfile.jpg)'}}></div>
                }
                <div className='fcs aic poc mr10'>
                    <div className='poname'>Posts</div>
                    <div className='ponum'>{posts.length}</div>
                </div>
                <div className='fcs aic poc mr10'>
                    <div className='poname'>Followers</div>
                    <div className='ponum'>{flrs.length}</div>
                </div>
                <div className='fcs aic poc mr10'>
                    <div className='poname'>Following</div>
                    <div className='ponum'>{flgs.length}</div>
                </div>
            </div>
            <div className='mt10 ml10 fontrr fs12 fwb w60'>here is bio content! freedom</div>
            <div className='frc w100 mt20'>
                {isFollow?
                <div className='mainPageOPtion fcc fs12 fontrr' ref={followTag} onClick={following}>unfollow</div>
                :
                <div className='mainPageOPtion fcc fs12 fontrr bclogo' ref={followTag} onClick={following}>Follow</div>    
                }
                <div 
                className='mainPageOPtion fcc fs12 fontrr ml5'
                onClick={()=>{
                    openMessageRoot(state.username,state.id,profile_info.profile_picture)
                }}
                >Message</div>
            </div>
            <div id='postContent' className='frfs fww ais mt20 mb40'>
            {
                posts.map((item,i)=>{
                    return(
                        <SmallPost key={i} func={()=>{
                            openPostPageSmallPost(item,posts)
                        }} item={item.name}/>
                    )
                })
            }
            </div>
        </div>
    )
}

export default Profile;

const SmallPost = ({item,func}) => {
    return(
        <video className={`post`} src={`uploads/${item}`} onClick={func}></video>
    )
}