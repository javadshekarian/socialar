import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/chromaKey.css';

import {useEffect,useRef,useState} from 'react';
import {useNavigate} from 'react-router-dom';
import axios from 'axios';

function VideoChromaKey(){
    const navigate = useNavigate();

    const backToParent = ()=>{
        navigate(-1);
    }

    const [streamC,setStream] = useState(null);
    const [chunk,setChunk]   = useState([]);

    const imgVideo      = useRef(null);
    const canvasRef     = useRef(null);
    const mediaRecorder = useRef(null);

    const startRecording = () =>{
        const media = new MediaRecorder(streamC);;
        mediaRecorder.current = media;
        mediaRecorder.current.start();

        let localAudioChunks = [];
        mediaRecorder.current.ondataavailable = (event) => {
            if (typeof event.data === "undefined") return;
            if (event.data.size === 0) return;
            localAudioChunks.push(event.data);
        };

        setChunk(localAudioChunks);
    }

    const stopRecording =()=>{
        mediaRecorder.current.stop();

        mediaRecorder.current.onstop = async() =>{
            const blob = new Blob(chunk,{type:'video/webm'});
            const file = new File([blob],`${Date.now()}.webm`);

            var formdata = new FormData();
            formdata.append('file',file);
            formdata.append('token',sessionStorage.getItem('token'));

            await axios.post(`https://api.bychat.one/upload/video-chroma-key`,formdata)

            setChunk([]);
        }
    }

    const pauseRecording = () =>{
        mediaRecorder.current.pause();
    }

    const resumeRecording = () =>{
        mediaRecorder.current.resume();
    }

    useEffect(()=>{
        const video  = imgVideo.current;
        const canvas = canvasRef.current;
        const {clientWidth:w,clientHeight:h} = video;
        const ctx = canvas.getContext('2d');

        navigator.mediaDevices.getUserMedia({video:true,audio:true}).then(stream=>{
            video.srcObject = stream;
            video.play();
            setStream(canvasRef.current.captureStream())
        })

        canvas.width = w;
        canvas.height = h;

        video.addEventListener('play',drawVid);

        function drawVid(){
            ctx.drawImage(video,0,0,w,h);

            var frame = ctx.getImageData(0,0,w,h);

            for(let i=0;i<frame.data.length;i+=4){
                var r = frame.data[i];
                var g = frame.data[i+1];
                var b = frame.data[i+2];
                
                if(g>r+b){
                    frame.data[i+3] = 0;
                }
            }

            ctx.putImageData(frame,0,0);
            requestAnimationFrame(drawVid);
        }
    },[])

    return(
        <div id="VideoChromaKey">
            <div className="pa canvasImgContent">
                <canvas id="canvasImg" ref={canvasRef}></canvas>
            </div>
            <div className="pa">
                <video 
                autoPlay 
                muted={true} 
                ref={imgVideo}
                ></video>
            </div>
            <div className='pa frs w100' id='btnBox'>
                <button onClick={startRecording}>start</button>
                <button onClick={pauseRecording}>pause</button>
                <button onClick={resumeRecording}>resume</button>
                <button onClick={stopRecording}>stop</button>
            </div>
            <i className='bi bi-arrow-left pa mt10 ml10 fs24 cgray' onClick={backToParent}></i>
        </div>
    )
}

export default VideoChromaKey;