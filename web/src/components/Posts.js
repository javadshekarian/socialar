import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/home.css';
import '../css/App.css';
import {useNavigate,useLocation} from 'react-router-dom';
import {useState,useRef,useEffect} from 'react';
import LargPost from './LargPost';
import axios from 'axios';
import { jwtDecode } from 'jwt-decode';

function Posts({socket}){
    const {state} = useLocation();
    const [likeObj,setLikeObj] = useState([]);
    const [followingProfile,setFollowingProfile] = useState([]);
    const [sendList,setSendList] = useState({});
    const followingContainerSign = useRef(null);
    const [post_id_child,setPost_id_child] = useState('');
    const sendBtnTags = useRef(null);

    const useSetPost_id_child = (post_id) => {
        setPost_id_child(post_id)
    }

    useEffect(()=>{
        var elem = document.getElementById(state.videoName);
        window.scrollTo(elem.offsetLeft,elem.offsetTop-50);

        axios.post(`https://api.bychat.one/like/all-my-like`,{
            token:sessionStorage.getItem('token')
        }).then(res=>{
            var obj = res.data.reduce((obj, item) => (obj[item.post_id] = true, obj) ,{});
            setLikeObj(obj);
        })

        axios.post(`https://api.bychat.one/follow/load-followings`,{
            token:sessionStorage.getItem('token')
        }).then(res=>{
            setFollowingProfile(res.data)
        })
    },[])

    const p = state.type==='private';

    const navigate = useNavigate();
    const backToClicked = () =>{
        navigate(-1);
    }

    const openProfile = () =>{
        navigate('/profile',{state:{
            pageName:'posts'
        }})
    }

    const setInSendList = (event,username,user_id) => {
        var index = new Object();
        index[username] = user_id;
        const send_btn = event.target;

        if(sendList[username] === undefined){
            setSendList({...sendList,...index});
            send_btn.classList.add('bcw');send_btn.classList.add('cb');
            send_btn.classList.remove('w42');send_btn.innerText = 'unsend';
        }else{
            var obj = sendList;
            delete obj[username];
            setSendList(obj);
            send_btn.classList.remove('bcw');send_btn.classList.remove('cb');
            send_btn.classList.add('w42');send_btn.innerText = 'send';
        }
    }

    const sendPostToFollowings =() =>{
        followingContainerSign.current.style.marginTop = '100vh';
        const token = jwtDecode(sessionStorage.getItem('token'));
        
        socket.emit('send-post',{
            sendList:sendList,
            message:post_id_child,
            time:Date.now(),
            from:token.user_id,
            sender_username:token.username
        })

        var sendBtns = sendBtnTags.current.querySelectorAll('.sendPostBtn');
        sendBtns.forEach(sendBtn=>{
            sendBtn.classList.remove('bcw');sendBtn.classList.remove('cb');
            sendBtn.classList.add('w42');sendBtn.innerText = 'send';
        })

        setSendList([]);
    }

    return(
        <>
            <div className='pa'>
            {!!p?
            <div>
                <div className='w100 frfs'>
                    <i className='ml10 bi bi-arrow-left fs20' onClick={backToClicked}></i>
                </div>
                {state.posts.map((item,i)=>{
                    var isExist = !!item.reference.profile_picture?item.reference.profile_picture:'nonProfile.jpg';
                    return(
                        <LargPost user_id={item.user_id} useSetPost_id_child={useSetPost_id_child} sendList={sendList} socket={socket} followTag={followingContainerSign} profile_picture={isExist} username={item.username} isLike={!!likeObj[item._id]} key={i} p={p} post_id={item._id} item={item.name} func={openProfile}/>
                    )
                })}
            </div>    
            :
            <div>
                <div className='w100 frfs'>
                    <i className='ml10 bi bi-arrow-left fs20' onClick={backToClicked}></i>
                </div>
                {state.posts.map((item,i)=>{
                    var isExist = !!item.reference.profile_picture?item.reference.profile_picture:'nonProfile.jpg';
                    return(
                        <LargPost user_id={item.user_id} useSetPost_id_child={useSetPost_id_child} sendList={sendList} socket={socket} followTag={followingContainerSign} profile_picture={isExist} username={item.username} isLike={!!likeObj[item._id]} key={i} p={p} post_id={item._id} item={item.name} func={openProfile}/>
                    )
                })}
            </div> 
            }
            </div>
            <div className='pf followingContainer fontrr' ref={followingContainerSign} style={{marginTop:'100vh'}}>
                <div className='followingContainerTwo' ref={sendBtnTags}>
                    {followingProfile.map((item,i)=>{
                        var profile_pic = !!item.profile_picture?item.profile_picture:'nonProfile.jpg';
                        return(
                            <div className='frsb h50 aic' key={i}>
                                <div className='frfs aic ml5'>
                                    <div 
                                    className='profilePicture'
                                    style={{backgroundImage:`url(/profile_pictures/${profile_pic})`}}
                                    ></div>
                                    <div className='ml10 fs12'>{item.username}</div>
                                </div>
                                <div className='sendPostBtn w42 frc aic mr5 fs12' onClick={e=>{
                                    setInSendList(
                                        e,
                                        item.username,
                                        item._id
                                    )
                                }}>send</div>
                            </div>
                        )
                    })}
                </div>
                <div className='pa OKContent frc aic'>
                    <button id='confirmSendPosts' onClick={sendPostToFollowings}>OK</button>
                </div>
            </div>
        </>
    )
}

export default Posts;
