import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/login.css';

import {useNavigate} from 'react-router-dom';
import axios from 'axios';

function Register(){
    var navigate = useNavigate();

    const redToLogin = ()=>{
        navigate('/login',{replace:true});
    }

    return(
        <div id="container2" className='fcs aic'>
            <div className='circleS pa cs'></div>
            <div className='circleS2 pa cs'></div>
            <div className='circleS3 pa cs'></div>
            <div className='loginIcon mb10'></div>
            <div className='tac mt-20'>
                <div className='text1 logText'>Welcome!</div>
                <div className='text2 logText'>create account</div>
            </div>
            <form onSubmit={(e)=>{
                saveRegisterData(e,navigate);
            }}>
                <div 
                className='clogo fs14 mb20'
                onClick={redToLogin}
                >login</div>
                <div className='frfs ml40 pr'>
                    <input 
                    type="text" 
                    className='loginInput'
                    placeholder='Inter Your Username'
                    id="username"
                    />
                    <div className='iconContent frc aic'>
                        <i className="bi bi-person"></i>
                    </div>
                </div>
                <div className='frfs ml40 pr mt25'>
                    <input 
                    type="text" 
                    className='loginInput'
                    placeholder='Inter Your Email'
                    id="email"
                    />
                    <div className='iconContent frc aic'>
                        <i className="bi bi-envelope fs25"></i>
                    </div>
                </div>
                <div className='frfs ml40 pr mt24'>
                    <input 
                    type="text" 
                    className='loginInput'
                    placeholder='Inter Your Password'
                    id="password"
                    />
                    <div className='iconContent frc aic'>
                    <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 448 512">
                        <path fill='#00C8FF' d="M144 144v48H304V144c0-44.2-35.8-80-80-80s-80 35.8-80 80zM80 192V144C80 64.5 144.5 0 224 0s144 64.5 144 144v48h16c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V256c0-35.3 28.7-64 64-64H80z"/>
                    </svg>
                    </div>
                </div>
                <div className="error pa mt15 ml20 fs12 cr dsn" id="error">this is error content</div>
                <div className="frc mt50">
                    <button id='signup' type="submit">Sign Up</button>
                </div>
            </form>
        </div>
    )
}


async function saveRegisterData(e,navigate){
    e.preventDefault();

    var form = e.target;
    var email = form.querySelector('#email');
    var username = form.querySelector('#username');
    var password = form.querySelector('#password');
    var errorContent = form.querySelector('#error');

    try{
        var res = await axios.post(`https://api.bychat.one/register/register`,{
            email:email.value,
            username:username.value,
            password:password.value
        })
        if(res.status!==200){
            if(errorContent.classList.contains('cgreen')){
                errorContent.classList.remove('cgreen');
            }
            errorContent.classList.remove('dsn');
            errorContent.classList.add('cr');
            errorContent.innerText = res.data.content;
        }else{
            sessionStorage.setItem('token',res.data)
            navigate('/',{replace:true});
        }
    }catch(err){
        if(err){
            console.log(err);
        }
    }
}
export default Register;