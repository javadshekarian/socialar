import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/chatroom.css';
import '../css/App.css';

import ChatroomContact from './ChatRoomContact';
import Menu from './Menu';
import {useEffect,useState} from 'react';
import axios from 'axios';
import {jwtDecode} from 'jwt-decode'

function LargPost(props){
    const [contacts,setContacts] = useState([]);

    useEffect(()=>{
        axios.post(`https://api.bychat.one/message/query-contact`,{
            token:sessionStorage.getItem('token')
        }).then(res=>{
            setContacts(res.data);
        })
    },[])

    return(
        <div>
            <div id='usersContent' className='pa'>
                <div className='frfs fontrr aic'>
                    <div className='fs18 mt10 ml15'>@ {jwtDecode(sessionStorage.getItem('token')).username}</div>
                </div>
                <div className='frc aic mt20 mb20'>
                    <input id='explorUserInput' placeholder='Searching Users'/>
                    <button id='userButtonExplor'>search</button>
                </div>
                <div className='frfs aic contactImagesContentChatroom mt10'>
                    {
                        contacts.map((item,i)=>{
                            var profile_pic = !!item.profile_picture?item.profile_picture:'nonProfile.jpg';
                            return(
                                <div key={i} style={{backgroundImage:`url(/profile_pictures/${profile_pic})`}} className={`contactImgChatroom ml5`}></div>
                            )
                        })
                    }
                </div>
                <div id='contactBox' className='frc fww pa mt10 mb40'>
                    {
                        contacts.map((item,i)=>{
                            console.log(item);
                            var profile_pic = !!item.profile_picture?item.profile_picture:'nonProfile.jpg';
                            return(
                                <ChatroomContact profile_picture={profile_pic} username={item.username} user_id={item._id} key={i}/>
                            )
                        })
                    }
                </div>
            </div>
            <Menu clsName='bi-send'/>
        </div>
    )
}

export default LargPost;