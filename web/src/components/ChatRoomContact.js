import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/chatroom.css';

import {useNavigate} from 'react-router-dom';

function ChatroomContact(props){
    const navigate = useNavigate();

    const openMessenger = (username,user_id,profile_picture) =>{
        console.log(username,user_id,profile_picture);
        navigate('/messenger',{state:{
            username:props.username,
            user_id:props.user_id,
            profile_picture:props.profile_picture
        }});
    }
    return(
        <div className='contactChatroom frsb aic mt5' onClick={()=>{
            openMessenger(props.username,props.user_id,props.profile_picture)
        }}>
            <div className='ml5 frfs aic fontrr fs14'>
                <div className='profilePictureChatroom' style={{backgroundImage:`url(/profile_pictures/${props.profile_picture})`}}></div>
                <div className='fcs ml5'>
                    <div className='fs12'>{props.username}</div>
                    <div className='fs12 mt5 cg'>last message content</div>
                </div>
            </div>
            <div className='fcs aic mr5'>
                <i className='bi bi-heart-fill'></i>
                <div className='fs12'>6:24</div>
            </div>
        </div>
    )
}

export default ChatroomContact;