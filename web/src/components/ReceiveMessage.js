import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/messenger.css';

function ReceiveMessage(props){
    return(
        <div className='frfs w100'>
            <div className='receiveMessage ml10'>{props.item.message}</div>
        </div>
    )
}

export default ReceiveMessage;