import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/explor.css';
import '../css/App.css';

import {useNavigate} from 'react-router-dom';

function Menu(props){
    var biPersionCircle = Boolean(props.clsName === 'bi-person-circle');
    var biHouse = Boolean(props.clsName === 'bi-house');
    var biPlusCircle = Boolean(props.clsName === 'bi-plus-circle');
    var biBagPlus = Boolean(props.clsName === 'bi-bag-plus');
    var biSend = Boolean(props.clsName === 'bi-send');

    const navigate = useNavigate();

    const openHomePage = () =>{
        navigate('/home',{replace:true});
    }

    const openCreateAR = () =>{
        navigate('/create-ar',{replace:true});
    }

    const openExplor = () =>{
        navigate('/explor',{replace:true});
    }

    const openChatroom = () =>{
        navigate('/chatroom',{replace:true});
    }

    const openProfile = () =>{
        navigate('/',{replace:true});
    }

    return(
        <div id='menu' className='frs aic'>
            <i 
            className={`bi bi-person-circle ${biPersionCircle?'menuActive':''}`}
            onClick={openProfile}
            style={{color:'#585858'}}
            ></i>
            <i 
            className={`bi bi-house ${biHouse?'menuActive':''}`} 
            onClick={openHomePage}
            ></i>
            <i 
            className={`bi bi-plus-circle ${biPlusCircle?'menuActive':''}`} 
            onClick={openCreateAR}
            ></i>
            <i 
            className={`bi bi-bag-plus ${biBagPlus?'menuActive':''}`} 
            onClick={openExplor}
            ></i>
            <i 
            className={`bi bi-send ${biSend?'menuActive':''}`} 
            onClick={openChatroom}
            ></i>
        </div>
    )
}

export default Menu;