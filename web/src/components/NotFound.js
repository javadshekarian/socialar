import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import {useNavigate} from 'react-router-dom';

function NotFound(){
    var navigate = useNavigate();
    
    const backToParent=()=>{
        navigate(-1)
    };

    return(
        <div 
        id='container' 
        className='frc aic wh100d'
        style={{fontFamily:'monR',fontSize:'28px'}}
        >
            <i 
            className='bi bi-arrow-left pa'
            style={{top:'10px',left:'10px',fontSize:'24px'}}
            onClick={backToParent}
            ></i>
            <div>404 Page Not Exist!</div>
        </div>
    )
}

export default NotFound;