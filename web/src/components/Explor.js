import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/explor.css';
import '../css/App.css';

import ContentOne from './BoxContentOne';
import ContentTwo from './BoxContentTwo';
import Menu from './Menu';

import axios from 'axios';
import {useEffect,useState,useRef} from 'react';
import { useNavigate } from 'react-router-dom';

function Home(){
    const [searchResult,setSearchResult] = useState([]);
    const searchInp = useRef(null);

    useEffect(()=>{
    },[searchResult]);

    const explorInputChange = async(e) =>{
        var formdata = new FormData();
        formdata.append('search',e.target.value);
        formdata.append('token',sessionStorage.getItem('token'));

        const res = await axios.post(`https://api.bychat.one/register/search-user`,formdata);
        setSearchResult(res.data);

        if(e.target.value.length === 0){
            searchInp.current.classList.add('dsn');
        }else{
            searchInp.current.classList.remove('dsn');
        }
    }
    return(
        <div>
            <div id='explor' className='pa'>
                <div id='result' className='pa mb40 mt40'>
                    <ContentOne/>
                    <ContentTwo/>
                </div>
                <div className='fcs aic' style={{position:'fixed',backgroundColor:'white'}}>
                    <div id='searchBox' className='frc aic'>
                        <input id='explorSearchInput' placeholder='Searching Object' onChange={explorInputChange}/>
                        <button id='searchButtonExplor'>search</button>
                    </div>
                    <div className='fcfs' ref={searchInp}>
                        {searchResult.map((item,i)=>{
                            return(
                                <SearchResultComponent key={i} item={item}/>
                            )
                        })}
                    </div>
                </div>
            </div>
            <Menu clsName='bi-bag-plus'/>
        </div>
    )
}

export default Home;

const SearchResultComponent = (props) =>{
    const navigate = useNavigate();

    const username = props.item.username;
    const _id = props.item._id;

    const openUserNamePage =(un,id)=>{
        navigate('/profile',{state:{username:un,id:id}});
    }

    return(
        <div className='searchResultContent fs14 fontrr' onClick={()=>openUserNamePage(username,_id)}>@{props.item.username}</div>
    )
}