import {useEffect,useState,useRef} from 'react';
import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/App.css';
import {useNavigate} from 'react-router-dom';
import Menu from './Menu';
import axios from 'axios';
import {jwtDecode} from 'jwt-decode'

function App() {
  const uploadForm = useRef(null);
  const username = jwtDecode(sessionStorage.getItem('token')).username;
  const [posts,setPosts] = useState([]);
  const [flrs,setFlrs] = useState([]);
  const [flgs,setFlgs] = useState([]);
  const [profile_inf,setProfile_inf] = useState({});
  const [profilePictureName,setProfilePictureName] = useState('');
  
  useEffect(()=>{
    setProfile_inf(jwtDecode(sessionStorage.getItem('token')));
    function readPosts(){
      var formdata = new FormData();
      formdata.append('token',sessionStorage.getItem('token'));
      axios.post(`https://api.bychat.one/upload/read-profile-posts`,formdata)
      .then(res=>{
        setPosts(res.data);
      })

      axios.post(`https://api.bychat.one/follow/follower-numbers`,{
        token:sessionStorage.getItem('token')
      })
      .then(res=>{
        setFlrs(res.data);
      })

      axios.post(`https://api.bychat.one/follow/following-numbers`,{
        token:sessionStorage.getItem('token')
      })
      .then(res=>{
        setFlgs(res.data);
      })
    }
    readPosts();
  },[profilePictureName])

  const navigate = useNavigate();

  const openLoginPage = () =>{
    navigate('/login',{replace:true});
  }

  const openPostPageSmallPost = (item,posts) =>{
    navigate('/posts',{state:{videoName:item.name,type:'private',posts:posts,item:item}});
  }

  const handleSave = () =>{
    axios.post(`https://api.bychat.one/save/read-my_saves`,{
      token:sessionStorage.getItem('token')
    }).then(res=>{
      if(res.data.length>0){
        var videoName = res.data[0].name; var type = "public";
        if(res.status === 200) navigate('/posts',{state:{videoName,type,posts:res.data}});
      }else{
        alert("you dont't have any save yet!")
      }
    })
  }

  const uploadProfilePicture = async (e) => {
    e.preventDefault();
    const form = uploadForm.current;
    const file_input = form.querySelector('input[type="file"]');
    const file = file_input.files[0];

    if(file === null) return;

    const formdata = new FormData();
    formdata.append('token',sessionStorage.getItem('token'));
    formdata.append('file',file)

    var res = await axios.post(`https://api.bychat.one/upload/profile-picture`,formdata);
    if(res.status === 200){
      sessionStorage.setItem('token',res.data);
      setProfilePictureName(jwtDecode(res.data).profile_picture);
    }
  }

  const uploadSelectedFile = () => {
    const form = uploadForm.current;
    const submit_btn = form.querySelector('input[type="submit"]');
    submit_btn.click();
  }

  const setProfilePicture = () => {
    const form = uploadForm.current;
    const file_input = form.querySelector('input[type="file"]');
    file_input.click();
  }

  const options = [
    {optionName:'Posts',num:posts.length,func:()=>{console.log('Posts');}},
    {optionName:'Followers',num:flrs.length,func:()=>{console.log('Followers');}},
    {optionName:'Following',num:flgs.length,func:()=>{console.log('Following');}},
  ]

  return(
      <div id='container' className='fcsb'>
      <div className='head'>
        <div className='frc'>
          <div className='rightBox mbox'></div>
          <div className='whiteSpace'>
            <div className='mainIcon frc aic'>
              <div className='arTextLogo'>AR</div>
            </div>
          </div>
          <div className='leftBox mbox'></div>
          <i className="bi bi-grid-3x3-gap pa oth" onClick={handleSave}></i>
          <i 
          className="bi bi-box-arrow-right pa log"
          onClick={openLoginPage}
          ></i>
        </div>
      </div>
      <div id='center' className='fcc ais'>
        <div id='profileContent' className='frs w100'>
          <div className='fcs aic'>
            <form className='dsn' ref={uploadForm} onSubmit={uploadProfilePicture}>
              <input type='file' onChange={uploadSelectedFile}/>
              <input type='submit'/>
            </form>
            {/* <div id='profilePicture' style={{backgroundImage:`url(/profile_pictures/${profile_inf.profile_picture})`}} onClick={setProfilePicture}></div> */}
            {!!profile_inf.profile_picture?
            <div id='profilePicture' style={{backgroundImage:`url(/profile_pictures/${profile_inf.profile_picture})`}} onClick={setProfilePicture}></div>
            :
            <div id='profilePicture' style={{backgroundImage:'url(/profile_pictures/nonProfile.jpg)'}} onClick={setProfilePicture}></div>
            }
            <div id='profileInfo' className='fs14'>{username}</div>
          </div>
          {
            options.map((item,i)=>{
              return(
                <ProfileOption key={i} optionName={item.optionName} num={item.num} func={item.func}/>
              )
            })
          }
        </div>
        <div id='postContent' className='frfs fww ais mt10 mb40'>
          {posts.length>0?
            posts.map((item,i)=>{
              return(
                <SmallPost key={i} func={()=>{
                  openPostPageSmallPost(item,posts)
                }} item={item.name}/>
              )
            })
            :
            <div className='fcfe aic w100 h50 fs18 fontrb mt20' style={{marginTop:'120px'}}>Not Post Yet!</div>
          }
        </div>
      </div>
      <Menu clsName='bi-person-circle'/>
    </div>
  )
}

export default App;

const ProfileOption = ({optionName,num,func}) =>{
  return(
    <div className='fcs aic poc mr10' onClick={func}>
      <div className='poname'>{optionName}</div>
      <div className='ponum'>{num}</div>
    </div>
  )
}

const SmallPost = ({item,func}) => {
  return(
    <video className={`post`} id={item} onClick={func} src={`uploads/${item}`}></video>
  )
}