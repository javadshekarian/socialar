import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/messenger.css';

function SendMessage(props){
    return(
        <div className='frfe w100'>
            <div className='sendMessage mr10'>{props.item.message}</div>
        </div>
    )
}

export default SendMessage;