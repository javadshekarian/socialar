import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/home.css';
import { useState,useRef,useEffect } from 'react';
import {useNavigate} from 'react-router-dom';
import axios from 'axios';

function LargPost(props){
    const [isFollowed,setIsFollowed] = useState('');
    const followTag = useRef(null);

    useEffect(()=>{
        const follow_condition = ((props.isFollow===undefined)||(props.isFollow===false))?'follow':'unfollow';
        setIsFollowed(follow_condition);
    },[])

    const navigate = useNavigate();
    const sendInput = useRef(null);

    const sendComment = (e,id) =>{
        var content = e.target.querySelector('input').value;
        e.preventDefault();
        var formdata = new FormData();
        formdata.append('token',sessionStorage.getItem('token'));
        formdata.append('post_id',id);
        formdata.append('comment',content);

        axios.post(`https://api.bychat.one/comment/add-comment`,formdata);

        sendInput.current.value = '';
    }

    const readComments = (post_id) => {
        navigate('/comment',{state:{post_id:post_id}})
    }

    const saveHandler = (post_id,token) => {
        axios.post(`https://api.bychat.one/save/save`,{
            post_id,
            token
        }).then(res=>{
            console.log(res.data.content);
        })
    }

    const replaceToAr = (video_name) => {
        window.open(`https://api.bychat.one/ar?video_name=${video_name}`,'_self')
    }

    const likeHandler = (e,post_id,token) =>{
        var formdata = new FormData();
        formdata.append('post_id',post_id);
        formdata.append('token',token);
        axios.post(`https://api.bychat.one/like/like`,formdata);

        if(e.target.classList.contains('bi-heart-fill')){
            e.target.classList.add('bi-heart');
            e.target.classList.remove('bi-heart-fill');
        }else{
            e.target.classList.remove('bi-heart');
            e.target.classList.add('bi-heart-fill');
        }
    }

    const followTagShow = (post_id) => {
        props.followTag.current.style.marginTop = '100px';
        props.useSetPost_id_child(post_id);
    }

    const handleFollowingInLargPost = async(user_id) =>{
        var formdata = new FormData();
        formdata.append('token',sessionStorage.getItem('token'));
        formdata.append('user_id',user_id);

        var res = await axios.post(`https://api.bychat.one/follow/follow`,formdata);
        
        if(res.data.content === 'unfollow'){
            followTag.current.innerText = 'Follow';
        }else{
            followTag.current.innerText = 'unfollow';
        }
    }

    return(
        <div className='mainPostContent frc fww'>
            <div className='frsb w100 aic h50'>
                <div className='frfs ml10 mt10 aic'>
                    {!!props.profile_picture?
                    <div className='profilePictureFor' style={{backgroundImage:`url(/profile_pictures/${props.profile_picture})`}}></div>
                    :
                    <div className='profilePictureFor' style={{backgroundImage:`url(/profile_pictures/nonProfile.jpg)`}}></div>    
                    }
                    {props.p?
                        <div className='ml10 fontrr fs14' onClick={()=>navigate(-1)}>{props.username}</div>
                        :
                        <div className='ml10 fontrr fs14' onClick={()=>props.func()}>{props.username}</div>
                    }
                </div>
                {props.p?
                    <div className='followInPost fontrr mr10 fs12 mt10 dsn'>follow</div> 
                    :
                    <div 
                    className='followInPost fontrr mr10 fs12 mt10' 
                    onClick={()=>handleFollowingInLargPost(
                        props.user_id
                    )}
                    ref={followTag}
                    >{isFollowed}</div> 
                }
            </div>
            <div className='contentBoxP postw mt5'>
                <video className='videoLargPost' id={`${props.item}`} controls src={`uploads/${props.item}`}></video>
            </div>
            <div 
            className='frsb aic postw h30'
            >
                <div className='frs postContentOption fs20 mt2'>
                    {!props.isLike?
                    <i className="bi bi-heart" onClick={(e)=>{
                        likeHandler(e,props.post_id,sessionStorage.getItem('token'));
                    }}></i>
                    :
                    <i className="bi bi-heart-fill" onClick={(e)=>{
                        likeHandler(e,props.post_id,sessionStorage.getItem('token'));
                    }}></i>
                    }
                    <i className='bi bi-chat ml10' onClick={()=>{
                        readComments(props.post_id)
                    }}></i>
                    <i className='bi bi-badge-ar ml10' onClick={()=>{
                        replaceToAr(props.item)
                    }}></i>
                </div>
                <div className='mr10 fs20 postContentOption' style={{width:'50px'}}>
                    <i className='bi bi-save mr10' onClick={()=>{
                        saveHandler(props.post_id,sessionStorage.getItem('token'));
                    }}></i>
                    <i className='bi bi-send' onClick={()=>{
                        followTagShow(props.post_id)
                    }}></i>
                </div>
            </div>
            <div className='postw fs12 fontrr mt10'>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Asperiores sunt iusto amet. Esse excepturi fugit, ex earum iure cumque sequi! Reprehenderit fuga voluptates eius deleniti quis laudantium cum quasi tenetur.
            </div>
            <form className='postw mt5 mb10 mt15' onSubmit={(e)=>{
                sendComment(e,props.post_id)
            }}>
                <input className='commentInput' ref={sendInput} placeholder='Add Comment'/>
                <button type='submit' className='dsn'>post</button>
            </form>
        </div>
    )
}

export default LargPost;