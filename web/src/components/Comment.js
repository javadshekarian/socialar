import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/comment.css';

import {useNavigate,useLocation} from 'react-router-dom';
import {useEffect,useState} from 'react';
import axios from 'axios';

function Comment(){
    const navigate = useNavigate();
    const {state} = useLocation();
    var [comments,setComments] = useState([]);

    useEffect(()=>{
        axios.post(`https://api.bychat.one/comment/read-comment`,{
            post_id:state.post_id
        }).then(res=>{
            setComments(res.data);
        })
    },[comments])
    
    const backToParent = ()=>{
        navigate(-1)
    }

    const sendComment = (e,post_id) =>{
        var content = e.target.querySelector('input').value;
        e.preventDefault();
        var formdata = new FormData();
        formdata.append('token',sessionStorage.getItem('token'));
        formdata.append('post_id',post_id);
        formdata.append('comment',content);

        axios.post(`https://api.bychat.one/comment/add-comment`,formdata).then(res=>{
            var comment_array = comments;
            comment_array.push(res.data);
            setComments(comment_array)
        });
        e.target.querySelector('input').value = '';
    }

    return(
        <div id='comment' className='fcfs mt5'>
            <div className='fcfs pa mt40 pad40 fdcr'>
                {comments.map((item,i)=>{
                    var isFill = ((i%2)===0)?true:false;
                    return(
                        <CommentDetail isFill={isFill} key={i} item={item}/>
                    )
                })}
            </div>
            <div className='h40 w100 bcw frfs aic pf'>
                <i className='bi bi-arrow-left fs18 ml10' onClick={backToParent}></i>
            </div>
            <form 
            className='pf inpContentC frc aic' 
            onSubmit={e=>{
                sendComment(e,state.post_id)
            }}
            >
                <input placeholder='add comment' className='addCommentInput'/>
                <button type='submit' className='dsn'></button>
            </form>
        </div>
    )
}

export default Comment;

function CommentDetail({item,isFill}){
    return(
        <div className='frsb w100 aic mt10'>
            {!!item.reference.profile_picture?
            <div 
            className='commentImg ml5' 
            style={{backgroundImage:`url(/profile_pictures/${item.reference.profile_picture})`}}
            ></div>
            :
            <div 
            className='commentImg ml5' 
            style={{backgroundImage:`url(/profile_pictures/nonProfile.jpg)`}}
            ></div>
            }
            <div className='fcsb'>
                <div className='fs12 fontrr commentBoxs'>{item.comment}</div>
                <div className='frs mt5'>
                    <div className='fs10 cgray'>reply</div>
                    <div className='fs10 cgray'>see translation</div>
                </div>
                <div className='frfs cgray fs10 mt5 aic'>
                    <div className='line'></div>
                    <div className='ml5'>view 20 reply</div>
                </div>
            </div>
            <div className='fcfs commentOptionBox mt5 aic cgray'>
                {isFill?
                <i className='bi bi-heart fs14'></i>
                :
                <i className='bi bi-heart-fill fs14'></i>
                }
                <div className='fs10 mt5'>2094</div>
            </div>
        </div>
    )
}