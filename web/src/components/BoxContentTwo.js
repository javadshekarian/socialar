import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/explor.css';
import '../css/App.css';

import {useNavigate} from 'react-router-dom';

function ContentTwo(){
    const navigate = useNavigate();
    const openPost = () =>{
        navigate('/posts',{state:{pageName:'explor'}})
    }

    return(
        <div className='fcfs'>
            <div className='frfs'>
                <div className='frfs fww'>
                    <div className='halfBox i3' onClick={openPost}></div>
                    <div className='halfBox i4' onClick={openPost}></div>
                    <div className='halfBox i5' onClick={openPost}></div>
                    <div className='halfBox i6' onClick={openPost}></div>
                </div>
                <div>
                    <div className='bigBox i4' onClick={openPost}></div>
                </div>
            </div>
            <div className='contentTwo frfs'>
                <div className='halfBox i7' onClick={openPost}></div>
                <div className='halfBox i1' onClick={openPost}></div>
                <div className='halfBox i2' onClick={openPost}></div>
                <div className='halfBox i3' onClick={openPost}></div>
            </div>
        </div>
    )
}

export default ContentTwo;