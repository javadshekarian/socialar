import React from 'react';
import {Navigate} from 'react-router-dom';

function PrivateRoute({children}){
    var x = Boolean(sessionStorage.getItem('token'));
    return x?children:<Navigate to="/login"/>;
}

export default PrivateRoute;