import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/createAR.css';
import '../css/App.css';

import {useNavigate} from 'react-router-dom';

import Menu from './Menu';
import Cookies from 'js-cookie';
import {jwtDecode} from 'jwt-decode';

function CreateAR(){
    const navigate = useNavigate();

    const imgChromaKeyFunc = () => {
        window.open(`https://api.bychat.one/imgChromaKey?token=${sessionStorage.getItem('token')}`,'_self');
    }

    const aiFunc = async() => {
        Cookies.set('user_id',jwtDecode(sessionStorage.getItem('token')).user_id,{domain:`https://api.bychat.one/`});
        window.open(`https://api.bychat.one/?token=${sessionStorage.getItem('token')}`,'_self');
    }

    const title = [
        {name:'Transparent AI',cl:'aiARL',func:()=>{aiFunc()}},
        {name:'Chroma Key Photo',cl:'cameraARL',func:()=>{navigate(imgChromaKeyFunc())}},
        {name:'Chroma Key Video',cl:'videoARL',func:()=>{navigate('/videoChromaKey')}},
        {name:'Remove Specific Color',cl:'majicWandARL',func:()=>{navigate('/majicRemove')}},
    ]
    return(
        <div>
            <div id='container4' className='frs aic fww pa w100'>
              {/* {
                title.map((item,i)=>{
                    return(
                        <Btn key={i} title={item.name} cl={item.cl} func={item.func}/>
                    )
                })
              } */}
              <div style={{
                width:'100px',
                height:'100px',
                border:'1px solid lightgray',
                borderRadius:'10px',
                fontSize:'30px',
                color:'lightgray'
              }} className='frc aic' onClickCapture={()=>{window.open(`https://api.bychat.one/imgChromaKey?token=${sessionStorage.getItem('token')}`,"_self")}}>
                <i className='bi bi-plus'></i>
              </div>
            </div>
            <Menu clsName='bi-plus-circle'/>
        </div>
    )
}

export default CreateAR;

const Btn = ({title,cl,func})=>{
    return(
        <div className='arOptionBox fcs aic' onClick={func}>
            <div className='optionLogoAR iar2 frc aic'>
                <div className={cl}></div>
            </div>
            <div className='optionName fcc'>{title}</div>
        </div>
    )
}