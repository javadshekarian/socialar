import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/home.css';
import '../css/App.css';

import LargPost from './LargPost';
import Menu from './Menu';
import {useNavigate} from 'react-router-dom';
import {useEffect,useState,useRef} from 'react';
import axios from 'axios';
import {jwtDecode} from 'jwt-decode';

function Home(){
    const [followingPosts,setFollowingPosts] = useState([]);
    const [likeObj,setLikeObj] = useState([]);
    const [followingsInfo,setFollowingsInfo] = useState([]);
    const storyForm = useRef(null);
    var navigate = useNavigate();

    useEffect(()=>{
        var formdata = new FormData();
        formdata.append('token',sessionStorage.getItem('token'));
        axios.post(`https://api.bychat.one/follow/followings-posts`,formdata)
        .then(res=>{
            setFollowingPosts(res.data)
        })

        axios.post(`https://api.bychat.one/follow/load-followings`,{
            token:sessionStorage.getItem('token')
        }).then(res=>{
            setFollowingsInfo(res.data);
        })

        axios.post(`https://api.bychat.one/like/all-my-like`,{
            token:sessionStorage.getItem('token')
        }).then(res=>{
            var obj = res.data.reduce((obj, item) => (obj[item.post_id] = true, obj) ,{});
            setLikeObj(obj);
        })
    },[])

    const openUserNamePage =(un,id)=>{
        navigate('/profile',{state:{username:un,id:id}});
    }

    const addStory = () => {
        const form = storyForm.current;
        const file_input = form.querySelector('input[type="file"]');
        file_input.click();
    }

    const saveStory = () => {
        const submit_input = storyForm.current.querySelector('input[type="submit"]');
        submit_input.click();
    }

    const handleSubmit = e => {
        e.preventDefault();
        console.log('submit!');
    }

    return(
        <div>
            <form ref={storyForm} className='dsn' onSubmit={handleSubmit}>
                <input type='file' onChange={saveStory}/>
                <input type='submit'/>
            </form>
            <div id='container3' className='pa mb40'>
                <div id='storyContent'>
                    <div 
                    id='story' 
                    className='frs bcw aic'
                    style={{width:`${(followingsInfo.length + 1)*55}px`}}
                    >
                        <div 
                        className='storyPicture frc aic' 
                        style={{backgroundImage:`url(/profile_pictures/${jwtDecode(sessionStorage.getItem('token')).profile_picture})`}} 
                        onClick={addStory}
                        >
                            <i className='bi bi-plus fs24 clogo'></i>
                        </div>
                        {followingsInfo.map((item,i)=>{
                            const profileIMG = !!item.profile_picture?item.profile_picture:'nonProfile.jpg';
                            return(
                                <Story profile_picture={profileIMG} key={i} img={item}/>
                            )
                        })}
                    </div>
                </div>
                <div id='allPosts' className='frc fww'>
                    {followingPosts.map((item,i)=>{
                        return(
                            <LargPost user_id={item.user_id} isFollow={true} isLike={!!likeObj[item._id]} profile_picture={item.reference.profile_picture} username={item.username} key={i} p={false} post_id={item._id} item={item.name} func={()=>{
                                openUserNamePage(item.username,item.user_id);
                            }}/>
                        )
                    })}
                </div>
            </div>
            <Menu clsName='bi-house'/>
        </div>
    )
}

export default Home;

var Story = (props) =>{
    return(
        <div className='storyPicture' style={{backgroundImage:`url(/profile_pictures/${props.profile_picture})`}}></div>
    )
}