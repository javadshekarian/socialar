import '../../node_modules/bootstrap-icons/font/bootstrap-icons.css';
import '../css/fonts.css';
import '../css/setting.css';
import '../css/messenger.css';

import SendMessage from './SendMessage';
import ReceiveMessage from './ReceiveMessage';
import {useNavigate,useLocation} from 'react-router-dom';
import {useState,useEffect,useRef} from 'react';
import {jwtDecode} from 'jwt-decode';
import axios from 'axios';

function Messenger({socket}){
    const [token,setToken] = useState({});
    const [messages, setMessages] = useState([]);
    const messagesContentCurrent = useRef();
    const [i,setI] = useState(0);


    const {state} = useLocation();
    const navigate = useNavigate();

    useEffect(()=>{
        socket.on('message', (data) => {
            if(state.user_id === data.from){
                setMessages([...messages, data])
            }else{
                setMessages([...messages])
            }
        })

        socket.on('receive_post',data=>{
            setMessages([...messages, data])
        })

        function queryMessages(){
            axios.post(`https://api.bychat.one/message/query-message`,{
                from:jwtDecode(sessionStorage.getItem('token')).user_id,
                to:state.user_id
            }).then(res=>{
                setMessages([...res.data])
            })
        }
        if(messages.length === 0 && (i===0 || i === 1)){
            queryMessages()
        }
        setI(i+1);

        setToken(jwtDecode(sessionStorage.getItem('token')));

        messagesContentCurrent.current.scrollTo(0,messagesContentCurrent.current.scrollHeight);
    },[socket,messages])

    const openChatroom = () =>{
        navigate('/chatroom',{replace:true});
    }

    const sendMessage = e => {
        e.preventDefault();

        var input = e.target.querySelector('input');
        
        if(input.value){
            var my_message = {
                from:token.user_id,
                to:state.user_id,
                sender_username:token.username,
                receiver_username:state.username,
                message:input.value,
                time:Date.now(),
                type:"text"
            }
            
            socket.emit('message',{
                ...my_message
            })
    
            setMessages([...messages, my_message])
        }

        input.value = '';
    }

    const openProfileRoot = (user_id) => {
        axios.post(`https://api.bychat.one/register/user-info`,{
            user_id:user_id
        }).then(res=>{
            const {username,profile_picture} = res.data;
            navigate('/profile',{state:{username:username,id:user_id}})
        })
    }

    return(
        <div id='messenger' className='fcfs'>
            <div className='h50 frfs aic bcw bblg'>
                <i className='bi bi-arrow-left fs24 ml10' onClick={openChatroom}></i>
                <div className='profilePictureInMessenger ml10' style={{backgroundImage:`url(/profile_pictures/${state.profile_picture})`}}></div>
                <div className='fontrr fs14 ml10' onClick={()=>{
                    openProfileRoot(state.user_id)
                }}>{state.username}</div>
            </div>
            <div id='messages' ref={messagesContentCurrent}>
                {
                    messages.map((item,i)=>{
                        if(item.type === 'text'){
                            if(item.from === token.user_id){
                                return(
                                    <SendMessage item={item} key={i}/>
                                )
                            }else{
                                return(
                                    <ReceiveMessage item={item} key={i}/>
                                )
                            }
                        }else if(item.type === 'post'){
                            if(item.from === token.user_id){
                                return(
                                    <SendPost item={item} key={i}/>
                                )
                            }else{
                                return(
                                    <ReceivePost item={item} key={i}/>
                                )
                            }
                        }
                    })
                }
            </div>
            <form onSubmit={sendMessage}>
                <div className='h50 frc aic bcw btlg'>
                    <input id='messengerInput' placeholder='Message'/>
                    <button id='messengerSend'>send</button>
                </div>
                <button type='submit' className='dsn'></button>
            </form>
        </div>
    )
}

export default Messenger;

function SendPost(props){
    const navigate = useNavigate();

    const openPostPage = () =>{
        const posts = new Array();
        posts.push(props.item.reference);
        navigate('/posts',{state:{videoName:props.item.reference.name,type:'public',posts:posts,item:posts}});
    }

    return(
        <div className='w100 frfe mt5' onClick={()=>{
            openPostPage()
        }}>
            <div className='postContent fcs aic mr5'>
                <div className='frfs w100c fs12 fontrr'>
                    <div className='fcs mt5 cw'>
                        <div>{props.item.message}</div>
                        <div>{props.item.sender_username}</div>
                    </div>
                </div>
                <video src={`/uploads/${props.item.reference.name}`} className='postShow'></video>
                <div className='w100c frfe fs10 cw'>5:30</div>
            </div>
        </div>
    )
}

function ReceivePost(props){
    const navigate = useNavigate();

    const openPostPage = () =>{
        const posts = new Array();
        posts.push(props.item.reference);
        navigate('/posts',{state:{videoName:props.item.reference.name,type:'public',posts:posts,item:posts}});
    }

    return(
        <div className='w100 frfs mt5' onClick={()=>{
            openPostPage()
      }}>
            <div className='postContent fcs aic bcrp cbor ml5'>
                <div className='frfs w100c fs12 fontrr'>
                    <div className='fcs mt5'>
                        <div>{props.item.message}</div>
                        <div>{props.item.sender_username}</div>
                    </div>
                </div>
                <video src={`/uploads/${props.item.reference.name}`} className='postShow cbor'></video>
                <div className='w100c frfe fs10'>5:30</div>
            </div>
        </div>
    )
}