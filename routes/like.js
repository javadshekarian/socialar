const {Router} =require('express');
const router = new Router();

const controller = require('../controller/likeController');

router.post('/like',controller.like);
router.post('/all-my-like',controller.allMyLikes);

module.exports.router=router;