const {Router} =require('express');
const router = new Router();

const controller = require('../controller/commentController');

router.post('/add-comment',controller.addComment);
router.post('/read-comment',controller.readComments);

module.exports.router=router;