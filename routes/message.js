const {Router} =require('express');
const router = new Router();

const controller = require('../controller/messageController');

router.post('/query-contact',controller.queryContact);
router.post('/query-message',controller.queryMessage);

module.exports.router=router;