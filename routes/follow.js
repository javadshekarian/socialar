const {Router} =require('express');
const router = new Router();

const controller = require('../controller/followController');

router.post('/follow',controller.follow);
router.post('/is-follow',controller.isFollow);
router.post('/follower-numbers',controller.followerNumber);
router.post('/following-numbers',controller.followingNumber);
router.post('/follower-numbers-decode',controller.followerNumberDecode);
router.post('/following-numbers-decode',controller.followingNumberDecode);
router.post('/followings-posts',controller.followingsPosts);
router.post('/following-profile',controller.followingProfile);
router.post('/load-followings',controller.loadFollowings);

module.exports.router=router;