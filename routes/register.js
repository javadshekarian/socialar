const {Router} =require('express');
const router = new Router();

const controller = require('../controller/registerController');

router.post('/register',controller.register);
router.post('/login',controller.login);
router.post('/search-user',controller.searchUser);
router.post('/set-cookie',controller.setCookie);
router.post('/user-info',controller.userInfo);

module.exports.router=router;