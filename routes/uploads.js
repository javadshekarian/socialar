const {Router} =require('express');
const router = new Router();

const controller = require('../controller/uploadsController');

router.post('/img-chroma-key',controller.imgChromaKey);
router.post('/video-chroma-key',controller.videoChromaKey);
router.post('/read-profile-posts',controller.readProfilePosts);
router.post('/cantact-posts',controller.contactPosts);
router.post('/profile-picture',controller.uploadProfilePicture);
router.post('/poset-postID',controller.postPostID);

module.exports.router=router;