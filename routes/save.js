const {Router} =require('express');
const router = new Router();

const controller = require('../controller/saveController');

router.post('/save',controller.save);
router.post('/read-my_saves',controller.readMy_saves);

module.exports.router=router;