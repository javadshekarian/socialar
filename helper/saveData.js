const jwt = require('jsonwebtoken');

const {followerInsertOne} = require('../usage/follower');
const {followingInsertOne} =require('../usage/following');

module.exports.saveData = async(validator,model,data,res) =>{
    if(!validator.error){
        var result = new model(data);
        try{
            var saved = await result.save();

            const token = jwt.sign(
                { user_id: saved._id, email:saved.email ,username:saved.username},
                'javadshekarian1378',
                {
                    expiresIn:'2h'
                }
            )
            await followerInsertOne({user_id:saved._id});
            await followingInsertOne({user_id:saved._id});
            result.token = token;
            res.status(200).json(token);
        }catch(err){
            res.status(406).json({content:'data not saved!'})
        }
    }else{
        res.status(203).json({content:validator.error.details[0].message})
    }
}