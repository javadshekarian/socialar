const canvas = document.querySelector('canvas');
const video = document.querySelector('video');
const pictureTag = document.querySelector('i');
const flipCamera = document.getElementById('flipCamera');

const ctx = canvas.getContext('2d');

flipCamera.addEventListener('click',()=>{
    if(document.cookie.length === 0){
        document.cookie = 'camera=environment';
    }else{
        if(document.cookie.split('=')[1]==='user'){
            document.cookie = 'camera=environment';
        }else{
            document.cookie = 'camera=user';
        }
    }
    location.reload();
})

pictureTag.addEventListener('click',startRecording);

var req;
var chunks = [];

function startRecording() {
    stream = canvas.captureStream();
    rec = new MediaRecorder(stream);

    rec.ondataavailable = e => chunks.push(e.data);

    rec.onstop = e => exportVid(new Blob(chunks, {type: 'video/webm'}));

    rec.start();
    setTimeout(()=>rec.stop(), 200);
}

async function exportVid(blob) {
    const file = new File([blob],`${Date.now()}.webm`);
    const token = window.location.href.split('?')[1].split('=')[1];
    const formdata = new FormData();
    formdata.append('token',token);
    formdata.append('file',file);
    
    var ajax = new XMLHttpRequest();
    ajax.onloadend = e =>{
        console.log(e.target.responseText);
    }
    ajax.open('post','/upload/img-chroma-key');
    ajax.send(formdata);

    chunks = [];
}

const openWebcam = () =>{
    const {clientWidth:w,clientHeight:h} = video;
    
    if(document.cookie.length>0){
        navigator.mediaDevices.getUserMedia({
            audio:true,
            video:{
                facingMode:document.cookie.split('=')[1]
            }
        })
        .then(stream=>{
            alert('it is true')
            video.srcObject = stream;
            video.play();
        })
    }else{
        navigator.mediaDevices.getUserMedia({
            audio:true,
            video:{
                facingMode:'environment'
            }
        })
        .then(stream=>{
            alert('it is true')
            video.srcObject = stream;
            video.play();
        })
    }
    
    // var cookieValue = document.cookie.length;
    // var cameraSide = !!cookieValue?document.cookie.split('=')[1]:'environment';
    // navigator.mediaDevices.getUserMedia({
    //     audio:true,
    //     video:{
    //         facingMode:cameraSide
    //     }
    // }).then(stream=>{
    //     video.srcObject = stream;
    //     video.play();
    // })

    // navigator.mediaDevices.getUserMedia({video:true,audio:true}).then(stream=>{
    //     video.srcObject = stream;
    //     video.play();
    // })

    canvas.width = w;
    canvas.height = h;
}

openWebcam();

video.addEventListener('play',drawVid);

function drawVid(){
    const {clientWidth:w,clientHeight:h} = video;
    ctx.drawImage(video,0,0,w,h);

    var frame = ctx.getImageData(0,0,w,h);

    for(let i=0;i<frame.data.length;i+=4){
        var r = frame.data[i];
        var g = frame.data[i+1];
        var b = frame.data[i+2];
        
        if(g>r+b){
            frame.data[i+3] = 0;
        }
    }

    ctx.putImageData(frame,0,0);
    requestAnimationFrame(drawVid);
}

