const webcamEl = document.getElementById("webcam");
const canvasEl = document.getElementById("canvas");
const flipCamera = document.getElementById('flipCamera');
const afterLoad = document.getElementById('afterLoad');
const notLoad = document.getElementById('notLoad');

const pauseButton = document.getElementById('pause');
var current_url = window.location.href;
var token = current_url.split('?')[1].split('=')[1];

var stream;
var isPause = false;

const canvasStream = canvasEl.captureStream();

flipCamera.addEventListener('click',()=>{
    if(document.cookie.length === 0){
        document.cookie = 'camera=environment';
    }else{
        if(document.cookie.split('=')[1]==='user'){
            document.cookie = 'camera=environment';
        }else{
            document.cookie = 'camera=user';
        }
    }
    location.reload();
})

tf.setBackend("webgl").then(() => main());
async function main() {
    if(document.cookie.length>0){
        navigator.mediaDevices.getUserMedia({
            audio:true,
            video:{
                facingMode:document.cookie.split('=')[1]
            }
        })
        .then(s=>{
            stream = s;
            canvasStream.addTrack(stream.getAudioTracks()[0]);
            webcamEl.srcObject = stream;
            webcamEl.play();
        })
    }else{
        navigator.mediaDevices.getUserMedia({
            audio:true,
            video:{
                facingMode:'user'
            }
        })
        .then(s=>{
            stream = s;
            canvasStream.addTrack(stream.getAudioTracks()[0]);
            webcamEl.srcObject = stream;
            webcamEl.play();
        })
    }
    const net = await bodyPix.load();

    const flipHorizontal = true;
    const foregroundColor = {r: 0, g: 0, b: 0, a: 0};
    const backgroundColor = {r: 255, g: 255, b: 255, a: 255};
    const maskBlurAmount = 0;
    const opacity = 1;

    async function renderLoop(now, metadata) {
        webcamEl.width = metadata.width;
        webcamEl.height = metadata.height;
        
        const segmentation = await net.segmentPerson(webcamEl);
        notLoad.classList.add('dsn');
        afterLoad.classList.remove('dsn');
        let coloredPartImage = bodyPix.toMask(segmentation, foregroundColor, backgroundColor);

        canvasEl.width = metadata.width;
        canvasEl.height = metadata.height;
        bodyPix.drawMask(canvasEl, webcamEl, coloredPartImage, opacity, maskBlurAmount, flipHorizontal);

        var ctx = canvasEl.getContext('2d');

        var frame = ctx.getImageData(0,0,canvasEl.width,canvasEl.height);

        for(let i=0; i<frame.data.length;++i){
            let r = frame.data[i];
            let g = frame.data[i+1];
            let b = frame.data[i+2];
            let a = frame.data[i+3]

            if(r===255 && g===255 && (b===255 || b===0) && a===255){
                frame.data[i+3] = 0;
                frame.data[i]   = 0;
                frame.data[i+1] = 0;
                frame.data[i+2] = 0;
            }
        }

        ctx.putImageData(frame,0,0);

        webcamEl.requestVideoFrameCallback(renderLoop);
    }
    webcamEl.requestVideoFrameCallback(renderLoop);
}

var chunk = [];

const mediaSource = new MediaSource();
mediaSource.addEventListener('sourceopen',handleSourceOpen,false);

var mediaRecorder;
var recordedBlob;
var sourceBuffer;

function handleSourceOpen(e){
    sourceBuffer = mediaSource.addSourceBuffer('video/webm; codecs=vp8');
}

function handleDataAvailable(e){
    if(e.data && e.data.size>0){
        recordedBlob.push(e.data);
    }
}

function handleStop(e){
    const blob = new Blob(recordedBlob,{type:'video/webm'});
    const file = new File([blob],`${Date.now()}.webm`);

    var formdata = new FormData();
    formdata.append('file',file);
    formdata.append('token',token);

    var ajax = new XMLHttpRequest();
    ajax.onloadend = e =>{
        console.log(JSON.parse(e.target.responseText));
    }
    ajax.open('post','/upload/video-chroma-key');
    ajax.send(formdata)

    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    a.download = 'test.webm';
    document.body.appendChild(a);
}

var startButton = document.getElementById('start');
var stopButton = document.getElementById('stop');
var downloadBtn = document.getElementById('download');

startButton.addEventListener('click',()=>{
    document.getElementById('start').classList.remove('startLime');
    document.getElementById('start').querySelector('i').classList.remove('playLime');

    document.getElementById('stop').classList.add('stopRed');
    document.getElementById('stop').querySelector('.morab').classList.add('morRed');

    document.getElementById('download').classList.remove('setDnAdmin');
    document.getElementById('download').querySelector('i').classList.remove('setDnAdminT');

    // document.getElementById('pause').classList.add('pauseActive');
    // document.getElementById('pause').querySelector('i').classList.add('pauseIActive');

    flipCamera.disabled = true;

    stopButton.disabled = false;
    downloadBtn.disabled = true;
    startButton.disabled = true;
    pauseButton.disabled = false;

    let option = {mimeType:'video/webm'};
    recordedBlob=[];
    
    try{
        mediaRecorder = new MediaRecorder(canvasStream,option);
    }catch(e1){
        try{
            option = 'video/vp8';
            mediaRecorder = new MediaRecorder(canvasStream,option);
        }catch(e2){
            console.log(e2);
            return;
        }
    }
    
    mediaRecorder.onstop = handleStop;
    mediaRecorder.ondataavailable = handleDataAvailable;
    mediaRecorder.start(100);
})

stopButton.addEventListener('click',()=>{
    document.getElementById('stop').classList.remove('stopRed');
    document.getElementById('stop').querySelector('.morab').classList.remove('morRed');

    document.getElementById('start').classList.add('startLime');
    document.getElementById('start').querySelector('i').classList.add('playLime');

    document.getElementById('download').classList.add('setDnAdmin');
    document.getElementById('download').querySelector('i').classList.add('setDnAdminT');

    // document.getElementById('pause').classList.remove('pauseActive');
    // document.getElementById('pause').querySelector('i').classList.remove('pauseIActive');

    pauseButton.innerHTML = '';
    pauseButton.innerHTML = `
    <i class="bi bi-pause"></i>
    `;
    
    stopButton.disabled = true;
    downloadBtn.disabled = false;
    startButton.disabled = false;
    pause.disabled = true;

    isPause = false;

    flipCamera.disabled = false;

    mediaRecorder.stop();
})

downloadBtn.onclick = ()=>{
    const blob = new Blob(recordedBlob,{type:'video/webm'});
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    a.download = `${Date.now()}.webm`;
    document.body.appendChild(a);
    a.click();
    setTimeout(()=>{
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
    },100)
}

var p = 0;
setInterval(()=>{
    if((p%4)===0){
        document.getElementById('AR').classList.add('setARAdmin');
    }else{
        document.getElementById('AR').classList.remove('setARAdmin');
    }

    ++p;
},2000)

function pauseRecording() {
  mediaRecorder.pause();
}

function resumeRecording() {
  mediaRecorder.resume();
}

pauseButton.addEventListener('click', () => {
    if (pauseButton.querySelector('i').classList.contains('bi-pause')) {
        pauseRecording();
        pauseButton.innerHTML = '';
        pauseButton.innerHTML=`
        <i class="bi bi-play"></i>
        `;
    } else {
        resumeRecording();
        pauseButton.innerHTML = '';
        pauseButton.innerHTML = `
        <i class="bi bi-pause"></i>
        `;
    }
});

window.onresize=()=>{
    document.getElementById('popupContent').style.height = window.innerHeight+'px';
}

window.onload= ()=>{
    document.querySelector('video').style.height = window.innerHeight+'px';
    document.querySelector('button').parentNode.style.marginTop = window.innerHeight-60+'px'; 
    document.getElementById('popupContent').style.height = window.innerHeight+'px';
}
        
        
