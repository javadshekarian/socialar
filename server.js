const path = require('path');

const express      = require('express');
const bodyParser   = require('body-parser');
const cors         = require('cors');
const mongoose     = require('mongoose');
const upload       = require('express-fileupload');
const cookieParser = require('cookie-parser');
const {Server}           = require('socket.io');

mongoose.connect('mongodb://127.0.0.1:27017/arshop');

const app = express();

app.use(express.static(path.join(__dirname,'public')));
app.use(express.static(path.join(__dirname,'web')));
app.use(express.static(path.join(__dirname,'node_modules','bootstrap-icons','font')));

app.use(bodyParser.urlencoded({extended:false}));
app.use(express.json());
app.use(cors());
app.use(upload());
app.use(cookieParser());

app.use('/register',require('./routes/register').router);
app.use('/upload',require('./routes/uploads').router);
app.use('/follow',require('./routes/follow').router);
app.use('/like',require('./routes/like').router);
app.use('/comment',require('./routes/comment').router);
app.use('/save',require('./routes/save').router);
app.use('/message',require('./routes/message').router);

app.get('/',(req,res)=>{
  res.sendFile(path.join(__dirname,'public','index.html'));
})

app.get('/ar',(req,res)=>{
  res.sendFile(path.join(__dirname,'public','AR.html'));
})

app.get('/imgChromaKey',(req,res)=>{
  res.sendFile(path.join(__dirname,'public','getPicture.html'));
})

const server = require('http').Server(app)
const io = require('socket.io')(server, {
    cors:{
      origins:["*"],
      handlePreflightRequest:(req,res)=>{
        res.writeHead(200,{
          "Access-Control-Allow-Origin": "*",
          'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
          "Access-Control-Allow-Headers": "vi_chat",
          'Access-Control-Allow-Credentials': true
        });
        res.end();
      }
    }
});

require('./socket.io/server').socketServer(io);

server.listen(8080, () => {
    console.log('listening on *:8080');
})