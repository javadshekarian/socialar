const {Like} = require('../model/Like');

module.exports.likeFindOne = async(data)=>{
    var result = await Like.findOne(data);
    return result;
}

module.exports.likeFindMany = async(data)=>{
    var result = await Like.find(data);
    return result;
}

module.exports.likeInsertOne = async(data)=>{
    var result = new Like(data);
    await result.save();
    return result;
}

module.exports.likeInsertMany = async(data)=>{
    var result = new Like(data);
    await result.save();
    return result;
}