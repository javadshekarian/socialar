const {Message} = require('../model/Message');

module.exports.messageFindOne = async(data)=>{
    var result = await Message.findOne(data);
    return result;
}

module.exports.messageFindMany = async(data)=>{
    var result = await Message.find(data);
    return result;
}

module.exports.messageInsertOne = async(data)=>{
    var result = new Message(data);
    await result.save();
    return result;
}

module.exports.messageInsertMany = async(data)=>{
    var result =await Message.insertMany(data);
    return result;
}

module.exports.messageUpdateMany = async(condition,data)=>{
    var result = await Message.updateMany({...condition},{$set:{...data}});
    return result;
}