const {User} = require('../model/Users');

module.exports.userFindOne = async(data)=>{
    var result = await User.findOne(data);
    return result;
}

module.exports.userFindMany = async(data)=>{
    var result = await User.find(data);
    return result;
}

module.exports.userInsertOne = async(data)=>{
    var result = new User(data);
    await result.save();
    return result;
}

module.exports.userInsertMany = async(data)=>{
    var result = new User(data);
    await result.save();
    return result;
}

module.exports.userUpdateOne = async(filter,update)=>{
    var result = await User.findOneAndUpdate(filter, update);
    return result;
}

// module.exports.userInsertMany = async(data)=>{
//     var result = new Upload(data);
//     await result.save();
//     return result;
// }