const {Upload} = require('../model/Upload');

module.exports.uploadFindOne = async(data)=>{
    var result = await Upload.findOne(data).populate({path:"reference",select:['profile_picture']}).where().exec();
    return result;
}

module.exports.uploadFindMany = async(data)=>{
    var result = await Upload.find(data).populate({path:"reference",select:['profile_picture']}).where().exec();
    return result;
}

module.exports.uploadInsertOne = async(data)=>{
    var result = new Upload(data);
    await result.save();
    return result;
}

module.exports.uploadInsertMany = async(data)=>{
    var result = new Upload(data);
    await result.save();
    return result;
}