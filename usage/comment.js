const {Comment} = require('../model/Comment');

module.exports.commentFindOne = async(data)=>{
    var result = await Comment.findOne(data);
    return result;
}

module.exports.commentFindMany = async(data)=>{
    var result = await Comment.find(data).populate({path:"reference"});
    return result;
}

module.exports.commentInsertOne = async(data)=>{
    var result = new Comment(data);
    await result.save();
    return result;
}

module.exports.commentInsertMany = async(data)=>{
    var result = new Comment(data);
    await result.save();
    return result;
}