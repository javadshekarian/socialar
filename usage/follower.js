const {Follower} = require('../model/Follower');

module.exports.followerFindOne = async(data)=>{
    var result = await Follower.findOne(data);
    return result;
}

module.exports.followerFindMany = async(data)=>{
    var result = await Follower.find(data);
    return result;
}

module.exports.followerInsertOne = async(data)=>{
    var result = new Follower(data);
    await result.save();
    return result;
}

module.exports.followerInsertMany = async(data)=>{
    var result = new Follower(data);
    await result.save();
    return result;
}