const {Save} = require('../model/Save');

module.exports.saveFindOne = async(data)=>{
    var result = await Save.findOne(data);
    return result;
}

module.exports.saveFindMany = async(data)=>{
    var result = await Save.find(data);
    return result;
}

module.exports.saveInsertOne = async(data)=>{
    var result = new Save(data);
    await result.save();
    return result;
}

module.exports.saveInsertMany = async(data)=>{
    var result = new Save(data);
    await result.save();
    return result;
}