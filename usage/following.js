const {Following} = require('../model/Following');

module.exports.followingFindOne = async(data)=>{
    var result = await Following.findOne(data);
    return result;
}

module.exports.followingFindMany = async(data)=>{
    var result = await Following.find(data);
    return result;
}

module.exports.followingInsertOne = async(data)=>{
    var result = new Following(data);
    await result.save();
    return result;
}

module.exports.followingInsertMany = async(data)=>{
    var result = new Following(data);
    await result.save();
    return result;
}