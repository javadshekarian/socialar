const Joi = require('joi');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {jwtDecode} = require('jwt-decode');

const {User} = require('../model/Users');
const {userFindMany,userFindOne,userInsertMany,userInsertOne} =require('../usage/user');

const {saveData} =require('../helper/saveData');

module.exports.register = async(req,res)=>{
    var result = await userFindOne({email:req.body.email});
    var hash = await bcrypt.hash(req.body.password,10);
    req.body.password = hash;
    
    if(!result){
        const validationSchema = Joi.object().keys({
            username:Joi.string().trim().min(3).max(80).required(),
            password:Joi.string().trim().min(3).max(80).required(),
            email:Joi.string().trim().email().required()
        })
    
        var isValid = validationSchema.validate(req.body);
        
        saveData(isValid,User,req.body,res);
    }else{
        res.status(208).json({content:'data is exist'});
    }
}

module.exports.login = async(req,res)=>{
    var result = await userFindOne({username:req.body.username});
    if(!result){
        res.status(203).json({content:'user not exist!'})
    }else{
        bcrypt.compare(req.body.password,result.password,(err,data)=>{
            if (data) {
                const token = jwt.sign(
                    { 
                        user_id: result._id, 
                        email:result.email,
                        username:result.username,
                        profile_picture:result.profile_picture
                    },
                    'javadshekarian1378',
                    {
                        expiresIn:'2h'
                    }
                )
                result.token = token;
                res.status(200).json(token);
            } else {
                res.status(203).json({ content: "Invalid credencial!" })
            }
        })
    }
}

module.exports.searchUser = async(req,res)=>{
    var searchContent = req.body.search;
    var token = jwtDecode(req.body.token);

    var result = await User.find({username:{$regex:new RegExp(searchContent,"i"),$ne:token.username}}).select({username:1,_id:1});

    if(!result){
        res.status(203).json({content:'not find user'});
    }else{
        res.status(200).json(result);
    }
}

module.exports.setCookie = async(req,res) => {
    res.redirect('/')
}

module.exports.userInfo = async (req,res) => {
    try{
        const {user_id} = req.body;
        const user = await userFindOne({_id:user_id});
        res.status(200).json(user);
    }catch(err){
        res.status(500).json({content:err});
    }
}