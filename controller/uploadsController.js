const {generate} =require('shortid');
const {jwtDecode} = require('jwt-decode');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const {uploadInsertOne,uploadFindMany, uploadFindOne} = require('../usage/upload');
const {userFindOne, userUpdateOne} = require('../usage/user');
const {likeFindOne, likeInsertOne} = require('../usage/like');
const {Upload} = require('../model/Upload');

module.exports.imgChromaKey = async(req,res)=>{
    var file = req.files.file;
    var token = jwtDecode(req.body.token);

    var fileName = `img_${generate()}.webm`;

    var user = await userFindOne({_id:token.user_id});

    if(!user){
        res.status(203).json({content:'user not exist!'})
    }else{
        file.mv(`./web/public/uploads/${fileName}`,async (err)=>{
            
            if(err){
                res.status(500).json({content:'file not saved!'});
            }else{
                var data = {
                    username:token.username,
                    email:token.email,
                    user_id:token.user_id,
                    name:fileName,
                    reference:token.user_id
                };
    
                const post = await uploadInsertOne(data);
                await likeInsertOne({
                    post_id:post._id
                })
                res.status(200).json({content:"image is saved!"})
            }
        })
    }
}

module.exports.videoChromaKey = async(req,res)=>{
    var file = req.files.file;
    var token = jwtDecode(req.body.token);

    var fileName = `video_${generate()}.webm`;

    var user = await userFindOne({_id:token.user_id});

    if(!user){
        res.status(203).json({content:'user not exist!'})
    }else{
        file.mv(`./web/public/uploads/${fileName}`,async (err)=>{
            if(err){
                res.status(500).json({content:'file not saved!'});
            }else{
                var data = {
                    username:token.username,
                    email:token.email,
                    user_id:token.user_id,
                    name:fileName,
                    type:'video',
                    reference:token.user_id
                };
    
                const post = await uploadInsertOne(data);
                await likeInsertOne({
                    post_id:post._id
                })
                res.status(200).json({content:"video is saved!"})
            }
        })
    }   
}

module.exports.readProfilePosts = async (req,res)=>{
    var token = jwtDecode(req.body.token);
    var user = await userFindOne({_id:token.user_id});

    if(!user){
        res.status(203).json({content:'user not exist!'});
    }else{
        var posts = await uploadFindMany({user_id:token.user_id});
        res.status(200).json(posts);
    }
}

module.exports.contactPosts = async(req,res)=>{
    var {id} = req.body;

    var result = await uploadFindMany({user_id:id});
    res.status(200).json(result);
}

module.exports.uploadProfilePicture = async(req,res)=>{
    const file = req.files.file;
    const token = jwtDecode(req.body.token);
    
    const fileType = file.name.split('.').reverse()[0];
    const corrent_type = (fileType === 'png') || (fileType === 'jpg') || (fileType === 'jpeg');
    if(corrent_type){
        const fileName = `${generate()}.${fileType}`;
        await file.mv(`./web/public/profile_pictures/${fileName}`);
        var result = await userUpdateOne({_id:token.user_id},{profile_picture:fileName});
        
        const new_token = jwt.sign(
            { 
                user_id: result._id,
                email:result.email ,
                username:result.username,
                profile_picture:fileName
            },
            'javadshekarian1378',
            {
                expiresIn:'24h'
            }
        )

        res.status(200).json(new_token);
    }else{
        res.status(203).json({content:"type error"})
    }
}

module.exports.postPostID = async (req,res) => {
    try{
        const {post_id} = req.body;
        const post = await uploadFindOne({_id:post_id});
        res.status(200).json(post);
    }catch(err){
        res.status(500).json({content:"fail"});
    }
}