const {jwtDecode} = require('jwt-decode');

const {followingFindMany} = require('../usage/following');
const {User} = require('../model/Users');
const {Message} = require('../model/Message');

module.exports.queryContact = async (req,res) => {
    const token    = jwtDecode(req.body.token);
    const user_id  = token.user_id;
    
    var followings = await followingFindMany({user_id});
    if(followings.length>0){
        followings = followings[0].following;

        try{
            var select = {_id:1,username:1,profile_picture:1,email:1}
            var contacts = await User.find().where('_id').in(followings).select(select).exec();
            res.status(200).json(contacts);
        }catch(err){
            res.status(500).json({content:"fail"});
        }
    }else{
        res.status(200).json({})
    }
    
}

module.exports.queryMessage = async (req,res) => {
    try{
        const {from,to} = req.body;
        const result = await Message.find({$or:[
            {from:from,to:to},
            {from:to,to:from}
        ]}).populate({path:'reference',populate:{path:'reference'}})
        .sort({'submittedDate': 'desc'})
        .exec();

        res.status(200).json(result);
    }catch(err){
        console.log(err);
        res.status(500).json({content:"fail"})
    }
}