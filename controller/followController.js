const {jwtDecode} = require('jwt-decode');
const {Following} = require('../model/Following');
const {Follower} = require('../model/Follower');
const {followingFindMany} = require('../usage/following');
const {followerFindMany} = require('../usage/follower');
const {Upload} = require('../model/Upload');
const {User} = require('../model/Users');
const {followingFindOne} = require('../usage/following');

module.exports.follow = async(req,res)=>{
    var token = jwtDecode(req.body.token);
    const my_id = token.user_id;

    var isFollow = await Following.find({  
        user_id:my_id
        ,
        "following": req.body.user_id
    });

    if(isFollow.length){
        await Following.findOneAndUpdate({ user_id:my_id }, { "$pull": {"following":req.body.user_id} });
        await Follower.findOneAndUpdate({ user_id:req.body.user_id }, { "$pull": {"followers":my_id} });
        res.status(200).json({content:'unfollow'});
    }else{
        await Following.findOneAndUpdate({user_id:my_id},{$addToSet:{following:req.body.user_id}});
        await Follower.findOneAndUpdate({user_id:req.body.user_id},{$addToSet:{followers:my_id}});
        res.status(200).json({content:'Follow'});
    }
}

module.exports.isFollow = async(req,res)=>{
    var token = jwtDecode(req.body.token);
    const my_id = token.user_id;
    const user_id = req.body.id;

    var isFollow = await Following.find({  
        user_id:my_id
        ,
        "following": user_id
    });

    res.status(200).json(isFollow);
}

module.exports.followerNumber = async(req,res)=>{
    var token = jwtDecode(req.body.token);
    var result = await followerFindMany({user_id:token.user_id});
    res.status(200).json(result[0].followers)
}

module.exports.followingNumber = async(req,res)=>{
    var token = jwtDecode(req.body.token);
    var result = await followingFindMany({user_id:token.user_id});
    res.status(200).json(result[0].following)
}

module.exports.followerNumberDecode = async(req,res)=>{
    const user_id = req.body.user_id;
    var result = await followerFindMany({user_id});
    res.status(200).json(result[0].followers)
}

module.exports.followingNumberDecode = async(req,res)=>{
    const user_id = req.body.user_id;
    var result = await followingFindMany({user_id});
    res.status(200).json(result[0].following)
}

module.exports.followingsPosts = async(req,res)=>{
    var token = jwtDecode(req.body.token);
    var result = await followingFindMany({user_id:token.user_id});
    if(result.length>0){
        var following = result[0].following;

        try{
            var posts = await Upload.find().populate({path:"reference",select:['profile_picture']})
            .where('user_id').in(following)
            .populate({path:"reference",select:['profile_picture']}).exec();

            res.status(200).json(posts);
        }catch(err){
            res.status(500).json({content:"server error!"});
        }
    }else{
        res.status(200).json([])
    }
}

module.exports.followingProfile = async (req,res) =>{
    try{
        const user_id = req.body.user_id;
        const user = await User.findOne({_id:user_id}).select({profile_picture:1,username:1,_id:0,email:1});
        res.status(200).json(user);
    }catch(err){
        res.status(500).json({content:"fail"})
    }
}

module.exports.loadFollowings = async (req,res) =>{
    try{
        const token = jwtDecode(req.body.token);
        const user_id = token.user_id;
        const result = await followingFindOne({user_id:user_id});
        const following = result.following;
        const fields = {_id:1,username:1,email:1,profile_picture:1};
        const users = await User.find().where('_id').in(following).select(fields).exec();
        res.status(200).json(users)
    }catch(err){
        res.status(203).json({content:"fail"})
    }
}