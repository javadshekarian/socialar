const {jwtDecode} = require('jwt-decode');

const {Like} = require('../model/Like');
const {likeFindMany} = require('../usage/like');

module.exports.like = async(req,res)=>{
    const token = jwtDecode(req.body.token);
    const user_id = token.user_id;
    const post_id = req.body.post_id;

    var isLike = await Like.find({  
        post_id:post_id
        ,
        "likes": user_id
    });

    if(isLike.length){
        await Like.findOneAndUpdate({ post_id:post_id }, { "$pull": {"likes":user_id} });
        res.status(200).json({content:'unlike'});
    }else{
        await Like.findOneAndUpdate({post_id:post_id},{$addToSet:{likes:user_id}});
        res.status(200).json({content:'like'});
    }
}

module.exports.allMyLikes = async(req,res)=>{
    var user_id = jwtDecode(req.body.token).user_id;
    try{
        var result = await Like.find({ "likes": { $in: user_id } }).select({post_id:1,_id:0});
        res.status(200).json(result);
    }catch(err){
        res.status(500).json({content:err});
    }
}