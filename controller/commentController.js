const {jwtDecode} = require('jwt-decode');

const {commentInsertOne,commentFindMany} = require('../usage/comment');

module.exports.addComment = async(req,res)=>{
    const user_id = jwtDecode(req.body.token).user_id;
    const comment = req.body.comment;
    const post_id = req.body.post_id;
    const username = jwtDecode(req.body.token).username;
    const reference = user_id;

    try{
        var result = await commentInsertOne({user_id,comment,post_id,username,reference});
        res.status(200).json(result);
    }catch(err){
        res.status(500).json({content:'fail'});
    }
}

module.exports.readComments = async(req,res)=>{
    const post_id = req.body.post_id;

    try{
        var comments = await commentFindMany({post_id:post_id});
        res.status(200).json(comments);
    }catch(err){
        res.status(500).json({content:'fail'});
    }
}