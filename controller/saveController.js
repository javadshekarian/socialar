const {jwtDecode} = require('jwt-decode');

const {Save} = require('../model/Save');
const {saveInsertOne,saveFindMany} = require('../usage/save');
const {Upload} = require('../model/Upload');

module.exports.save = async (req,res) => {
    const token = jwtDecode(req.body.token);
    const post_id = req.body.post_id;
    const user_id = token.user_id;

    try{
        var hasDocument = await Save.find({user_id:user_id});
        if(!hasDocument.length) await saveInsertOne({user_id:user_id});

        var isSaved = await Save.find({  
            user_id:user_id,
            "my_saves": post_id
        });

        if(isSaved.length){
            await Save.findOneAndUpdate({ user_id:user_id }, { "$pull": {"my_saves":post_id} });
            res.status(200).json({content:'unsave'});
        }else{
            await Save.findOneAndUpdate({user_id:user_id},{$addToSet:{my_saves:post_id}});
            res.status(200).json({content:'save'});
        }
    }catch(err){
        res.status(500).json({content:err});
    }
}

module.exports.readMy_saves = async(req,res) => {
    const user_id = jwtDecode(req.body.token).user_id;
    
    try{
        var doc = await saveFindMany({user_id});
        if(doc.length>0){
            var my_saves = doc[0].my_saves;
            var posts = await Upload.find().populate({path:"reference"}).where('_id').in(my_saves).exec();
            res.status(200).json(posts);
        }else{
            res.status(200).json([]);
        }
    }catch(err){
        res.status(500).json({content:err});
    }
}